﻿using System;
using System.Collections;

namespace AudioSteganographer
{
    public class AudioSteganographer
    {
        /// <summary>
        /// To embed message in career file. Message is an byte array converted from wav file.
        /// </summary>
        /// <param name="career"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public WavFile Embed(WavFile career, byte[] message)
        {
            //Checks message length is larger than career data or not.
            if (message.Length * 8 > (career.LeftStream.Count + career.RightStream.Count) * 4)
            {
                throw new Exception("Message size is too large to embed.");
            }

            career.LeftStream[0] = (short)(message.Length / 32767); //Storing quotient of message size...
            career.RightStream[0] = (short)(message.Length % 32767); //Storing reminder of message size...

            var careerIdx = 1;
            foreach (var messageByte in message)
            {
                var messageBits = new BitArray(messageByte); //Convert message byte to bit array...
                var leftTempByte1 = career.LeftStream[careerIdx];
                var rightTempByte1 = career.RightStream[careerIdx];
                var leftTempByte2 = career.LeftStream[careerIdx+1];
                var rightTempByte2 = career.RightStream[careerIdx+1];



                careerIdx += 2;
            }
            return new WavFile("");
        }

        public byte[] Extract(WavFile career)
        {
            return new byte[0];
        }
    }
}
