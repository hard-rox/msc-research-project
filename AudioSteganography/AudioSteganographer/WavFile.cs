﻿using System.Collections.Generic;
using System.IO;

namespace AudioSteganographer
{
    public class WavFile
    {
        public byte[] RiffId; // "riff"
        public uint Size;  // Size
        public byte[] WavId;  // Format
        public byte[] FmtId;  // Subchunk1ID
        public uint FmtSize; // Subchunk size
        public ushort Format; // format
        public ushort Channels; // no of channels
        public uint SampleRate; // Samplerate
        public uint BytePerSec;
        public ushort BlockSize;
        public ushort Bit;
        public byte[] DataId;// "data"
        public uint DataSize;
        public List<short> LeftStream;
        public List<short> RightStream;

        public BinaryReader Br;

        public WavFile(string filePath)
        {
            this.Br = new BinaryReader(new FileStream(filePath, FileMode.Open));

            this.RiffId = Br.ReadBytes(4);
            this.Size = Br.ReadUInt32();
            this.WavId = Br.ReadBytes(4);
            this.FmtId = Br.ReadBytes(4);
            this.FmtSize = Br.ReadUInt32();
            this.Format = Br.ReadUInt16();
            this.Channels = Br.ReadUInt16();
            this.SampleRate = Br.ReadUInt32();
            this.BytePerSec = Br.ReadUInt32();
            this.BlockSize = Br.ReadUInt16();
            this.Bit = Br.ReadUInt16();
            this.DataId = Br.ReadBytes(4);
            this.DataSize = Br.ReadUInt32();

            this.LeftStream = new List<short>();
            this.RightStream = new List<short>();
            for (var i = 0; i < this.DataSize / this.BlockSize; i++)
            {
                this.LeftStream.Add((short)Br.ReadUInt16());
                this.RightStream.Add((short)Br.ReadUInt16());
            }
            Br.Close();
        }
    }
}
