﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Steganography
{
    public static class AesHandler
    {
        public static byte[] AesEncryptBytes(byte[] clearBytes, string password)
        {
            var passBytes = Encoding.UTF8.GetBytes(password);
            var saltBytes = Encoding.UTF8.GetBytes(password.PadLeft(8, '0'));

            byte[] encryptedBytes;

            // create a key from the password and salt, use 32K iterations – see note
            var key = new Rfc2898DeriveBytes(passBytes, saltBytes, 32768);

            // create an AES object
            using (Aes aes = new AesManaged())
            {
                // set the key size to 256
                aes.KeySize = 256;
                aes.Key = key.GetBytes(aes.KeySize / 8);
                aes.IV = key.GetBytes(aes.BlockSize / 8);
                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, aes.CreateEncryptor(),
                        CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }
            return encryptedBytes;
        }

        public static byte[] AesDecryptBytes(byte[] cryptBytes, string password)
        {
            var passBytes = Encoding.UTF8.GetBytes(password);
            var saltBytes = Encoding.UTF8.GetBytes(password.PadLeft(8, '0'));

            byte[] clearBytes;

            // create a key from the password and salt, use 32K iterations
            var key = new Rfc2898DeriveBytes(passBytes, saltBytes, 32768);

            using (Aes aes = new AesManaged())
            {
                // set the key size to 256
                aes.KeySize = 256;
                aes.Key = key.GetBytes(aes.KeySize / 8);
                aes.IV = key.GetBytes(aes.BlockSize / 8);

                try
                {
                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cryptBytes, 0, cryptBytes.Length);
                            cs.Close();
                        }
                        clearBytes = ms.ToArray();
                    }
                }
                catch (Exception e)
                {
                    if(e.Message == "Padding is invalid and cannot be removed.")
                        throw new Exception("Unauthorized. Password doesn't matched.");
                    throw new Exception("Sorry!!! Looks like something went wrong on our end.");
                }
            }
            return clearBytes;
        }
    }
}
