﻿using System;
using System.Text;

namespace Steganography
{
    public class AudioSteganographer
    {
        /// <summary>
        /// To embed message in career file. Message is an byte array converted from wav file.
        /// </summary>
        /// <param name="career"></param>
        /// <param name="message"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public WavFile Embed(WavFile career, SecretMessage message, string password)
        {
            var messageBytes = AesHandler.AesEncryptBytes(message.MessageBytes, password);

            //Checks message length is larger than career data or not...
            //Though left and right stream is same size...
            if (career.LeftStream.Count < (messageBytes.Length * 2) + 1)
            {
                throw new Exception("Message size is too large to embed. This file can embed file atmost " + career.GetEmbedCapacity() + " Bytes sized.");
            }

            career.LeftStream[0] = (short)(messageBytes.Length / 32767); //Storing quotient of message size...
            career.RightStream[0] = (short)(messageBytes.Length % 32767); //Storing reminder of message size...

            career.LeftStream[1] = Convert.ToInt16(message.GetExtensionBitString().Substring(0, 16), 2);//Storing message extension...
            career.RightStream[1] = Convert.ToInt16(message.GetExtensionBitString().Substring(16, 16), 2); ; //Storing message extension...

            var careerIdx = 2;
            foreach (var messageByte in messageBytes)
            {
                var messageBitString = Convert.ToString(messageByte, 2).PadLeft(8, '0');
                var leftTempBitString1 = Convert.ToString(career.LeftStream[careerIdx], 2).PadLeft(16, '0');
                var rightTempBitString1 = Convert.ToString(career.RightStream[careerIdx], 2).PadLeft(16, '0');
                var leftTempBitString2 = Convert.ToString(career.LeftStream[careerIdx + 1], 2).PadLeft(16, '0');
                var rightTempBitString2 = Convert.ToString(career.RightStream[careerIdx + 1], 2).PadLeft(16, '0');

                leftTempBitString1 = leftTempBitString1.Remove(14, 1).Insert(14, messageBitString[0].ToString());
                leftTempBitString1 = leftTempBitString1.Remove(15, 1).Insert(15, messageBitString[1].ToString());
                rightTempBitString1 = rightTempBitString1.Remove(14, 1).Insert(14, messageBitString[2].ToString());
                rightTempBitString1 = rightTempBitString1.Remove(15, 1).Insert(15, messageBitString[3].ToString());
                leftTempBitString2 = leftTempBitString2.Remove(14, 1).Insert(14, messageBitString[4].ToString());
                leftTempBitString2 = leftTempBitString2.Remove(15, 1).Insert(15, messageBitString[5].ToString());
                rightTempBitString2 = rightTempBitString2.Remove(14, 1).Insert(14, messageBitString[6].ToString());
                rightTempBitString2 = rightTempBitString2.Remove(15, 1).Insert(15, messageBitString[7].ToString());

                var modLeft1 = Convert.ToInt16(leftTempBitString1, 2);
                var modRight1 = Convert.ToInt16(rightTempBitString1, 2);
                var modLeft2 = Convert.ToInt16(leftTempBitString2, 2);
                var modRight2 = Convert.ToInt16(rightTempBitString2, 2);

                career.LeftStream[careerIdx] = modLeft1;
                career.RightStream[careerIdx] = modRight1;
                career.LeftStream[careerIdx + 1] = modLeft2;
                career.RightStream[careerIdx + 1] = modRight2;

                careerIdx += 2;
            }
            return career;
        }

        public SecretMessage Extract(WavFile career, string password)
        {
            var messageSize = (career.LeftStream[0] * 32767) + career.RightStream[0];
            var dataBoundery = (messageSize * 2) + 1;

            var messageBytes = new byte[messageSize];

            var messageByteIdx = 0;
            for (var i = 2; i < dataBoundery; i += 2)
            {
                var messageByte = "";

                var leftTempBitString1 = Convert.ToString(career.LeftStream[i], 2).PadLeft(16, '0');
                var rightTempBitString1 = Convert.ToString(career.RightStream[i], 2).PadLeft(16, '0');
                var leftTempBitString2 = Convert.ToString(career.LeftStream[i + 1], 2).PadLeft(16, '0');
                var rightTempBitString2 = Convert.ToString(career.RightStream[i + 1], 2).PadLeft(16, '0');

                messageByte += leftTempBitString1[14];
                messageByte += leftTempBitString1[15];
                messageByte += rightTempBitString1[14];
                messageByte += rightTempBitString1[15];
                messageByte += leftTempBitString2[14];
                messageByte += leftTempBitString2[15];
                messageByte += rightTempBitString2[14];
                messageByte += rightTempBitString2[15];

                messageBytes[messageByteIdx++] = Convert.ToByte(messageByte, 2);
            }
            messageBytes = AesHandler.AesDecryptBytes(messageBytes, password);

            var result = new SecretMessage().CreateMessageFromBytes(messageBytes, GetSecretFileExtension(career));
            return result;
        }

        private string GetSecretFileExtension(WavFile career)
        {
            var bitString = Convert.ToString(career.LeftStream[1], 2).PadLeft(16, '0') +
                            Convert.ToString(career.RightStream[1], 2).PadLeft(16, '0');
            var bytes = new[]
            {
                Convert.ToByte(bitString.Substring(0, 8), 2),
                Convert.ToByte(bitString.Substring(8, 8), 2),
                Convert.ToByte(bitString.Substring(16, 8), 2),
                Convert.ToByte(bitString.Substring(24, 8), 2),
            };
            var exString = Encoding.UTF8.GetString(bytes);
            return exString.Replace('*', ' ').Trim();
        }
    }
}
