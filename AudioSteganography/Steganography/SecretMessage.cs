﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Steganography
{
    public class SecretMessage
    {
        public byte[] MessageBytes { get; private set; }
        public string Extension { get; private set; }
        public long MessageSize => MessageBytes.Length + Extension.Length;

        public SecretMessage CreateFileMessage(string filePath)
        {
            this.MessageBytes = File.ReadAllBytes(filePath);
            this.Extension = Path.GetExtension(filePath).Split('.')[1].PadLeft(4, '*');

            return this;
        }

        public SecretMessage CreateTextMessage(string text)
        {
            this.MessageBytes = Encoding.UTF8.GetBytes(text);
            this.Extension = "text";

            return this;
        }

        internal string GetExtensionBitString()
        {
            var bytes = Encoding.UTF8.GetBytes(Extension);
            var result = bytes.Aggregate("", (current, b) => current + Convert.ToString(b, 2).PadLeft(8, '0'));
            return result;
        }

        internal SecretMessage CreateMessageFromBytes(byte[] bytes, string extension)
        {
            this.MessageBytes = bytes;
            this.Extension = extension;
            return this;
        }
    }
}
