﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Steganography
{
    public class WavFile
    {
        public byte[] RiffId; // "riff"
        public uint Size;  // Size
        public byte[] WavId;  // Format
        public byte[] FmtId;  // Subchunk1ID
        public uint FmtSize; // Subchunk size
        public ushort Format; // format
        public ushort Channels; // no of channels
        public uint SampleRate; // Samplerate
        public uint BytePerSec;
        public ushort BlockSize;
        public ushort Bit;
        public byte[] DataId;// "data"
        public uint DataSize;
        public List<short> LeftStream;
        public List<short> RightStream;

        public string FileName;
        public long FileSize;

        public BinaryReader Br;

        public WavFile(string filePath)
        {
            this.FileName = Path.GetFileName(filePath);
            this.FileSize = new FileInfo(filePath).Length;
            this.Br = new BinaryReader(new FileStream(filePath, FileMode.Open));

            this.RiffId = Br.ReadBytes(4);
            this.Size = Br.ReadUInt32();
            this.WavId = Br.ReadBytes(4);
            this.FmtId = Br.ReadBytes(4);
            this.FmtSize = Br.ReadUInt32();
            this.Format = Br.ReadUInt16();
            this.Channels = Br.ReadUInt16();
            this.SampleRate = Br.ReadUInt32();
            this.BytePerSec = Br.ReadUInt32();
            this.BlockSize = Br.ReadUInt16();
            this.Bit = Br.ReadUInt16();
            this.DataId = Br.ReadBytes(4);
            this.DataSize = Br.ReadUInt32();

            this.LeftStream = new List<short>();
            this.RightStream = new List<short>();
            for (var i = 0; i < this.DataSize / this.BlockSize; i++)
            {
                this.LeftStream.Add((short)Br.ReadUInt16());
                this.RightStream.Add((short)Br.ReadUInt16());
            }
            Br.Close();
        }

        public void WriteFile(string path)
        {
            this.DataSize = (uint)Math.Max(LeftStream.Count, RightStream.Count) * 4;

            var fs = new FileStream(path, FileMode.Create, FileAccess.Write);
            var bw = new BinaryWriter(fs);

            bw.Write(this.RiffId);
            bw.Write(this.Size);
            bw.Write(this.WavId);
            bw.Write(this.FmtId);
            bw.Write(this.FmtSize);
            bw.Write(this.Format);
            bw.Write(this.Channels);
            bw.Write(this.SampleRate);
            bw.Write(this.BytePerSec);
            bw.Write(this.BlockSize);
            bw.Write(this.Bit);
            bw.Write(this.DataId);
            bw.Write(this.DataSize);

            for (var i = 0; i < this.DataSize / this.BlockSize; i++)
            {
                if (i < this.LeftStream.Count)
                    bw.Write((ushort)this.LeftStream[i]);
                else
                    bw.Write(0);

                if (i < this.RightStream.Count)
                    bw.Write((ushort)this.RightStream[i]);
                else
                    bw.Write(0);
            }

            fs.Close();
            bw.Close();
        }

        public int GetEmbedCapacity()
        {
            return ((this.LeftStream.Count - 2) / 2) - 100;
        }
    }
}
