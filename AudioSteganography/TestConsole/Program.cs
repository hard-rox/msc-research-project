﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using NAudio.Wave;
using Steganography;

namespace TestConsole
{
    class Program
    {
        IWaveIn wi;
        WaveFileWriter wfw;
        int length;


        static void Main(string[] args)
        {
            //new Program().wi_StartRecording();

            var steg = new AudioSteganographer();
            const string path = @"D:\Projects\MSc Research Project\Resources\TestCases";
            var filePaths = Directory.GetFiles(path);
            using (StreamWriter sw = File.AppendText(@"D:\Projects\MSc Research Project\Resources\TestCases\output.csv"))
            {
                sw.WriteLine("Career File Size,Message Type,Message Size,Password Length,Embedded,Embedding Time,Stego Career Size,Extracted,Extracting Time,Comment");
            }
            foreach (var filePath in filePaths)
            {
                if(Path.GetExtension(filePath) != ".wav") continue;
                foreach (var variable in filePaths)
                {
                    var password = new Random().Next(99999999);
                    var line = (new FileInfo(filePath).Length / 1048576.0).ToString("N3") + " MB,"
                               + Path.GetExtension(variable) + ","
                               + (new FileInfo(variable).Length / 1048576.0).ToString("N3") + " MB,"
                               + password.ToString().Length + ",";

                    try
                    {
                        var stp = Stopwatch.StartNew();
                        var file = steg.Embed(new WavFile(filePath), new SecretMessage().CreateFileMessage(variable),
                            password.ToString());
                        stp.Stop();
                        line += "✓," + (stp.ElapsedMilliseconds / 1000.0).ToString("N3") + " Sec,";
                        file.WriteFile("out.wav");
                        line += (new FileInfo("out.wav").Length / 1048576.0).ToString("N3") + " MB,";

                        try
                        {
                            stp = Stopwatch.StartNew();
                            var result = steg.Extract(new WavFile("out.wav"), password.ToString());
                            stp.Stop();
                            line += "✓," + (stp.ElapsedMilliseconds / 1000.0).ToString("N3") + " Sec, Successfull";
                        }
                        catch (Exception e)
                        {
                            line += "✘,-,Unsuccessfull";
                        }
                    }
                    catch (Exception e)
                    {
                        line += "✘,-,-,✘,-,Unsuccessfull";
                    }

                    Console.WriteLine(line);
                    using (StreamWriter sw = File.AppendText(@"D:\Projects\MSc Research Project\Resources\TestCases\output.csv"))
                    {
                        sw.WriteLine(line);
                    }
                }
            }
            

            Console.WriteLine("Exiting...");
            Console.ReadLine();
        }
        Program() { }


        void wi_StartRecording()
        {
            wi = new WaveIn(WaveCallbackInfo.FunctionCallback());
            int devcount = WaveIn.DeviceCount;
            Console.Out.WriteLine("Device Count: {0}.", devcount);
            for (int c = 0; c < devcount; ++c)
            {
                WaveInCapabilities info = WaveIn.GetCapabilities(c);
                Console.Out.WriteLine("{0}, {1}", info.ProductName, info.Channels);
            }


            //If I use this instance, I can not set the sampling rate because
            //by defaut, the device does not support it.
            //wi = new WasapiCapture();


            wi.DataAvailable += new EventHandler<WaveInEventArgs>(wi_DataAvailable);
            wi.RecordingStopped += new EventHandler<StoppedEventArgs>(wi_RecordingStopped);
            wi.WaveFormat = new WaveFormat(8000, 1);


            wfw = new WaveFileWriter(@"record.wav", wi.WaveFormat);


            wi.StartRecording();
            Console.In.ReadLine();


            wfw.Dispose();
        }


        void wi_DataAvailable(object sender, WaveInEventArgs e)
        {
            wfw.Write(e.Buffer, 0, e.BytesRecorded);
            int seconds = (int)(wfw.Length / wfw.WaveFormat.AverageBytesPerSecond);
            length = seconds;
            if (seconds > 15)
            {
                wi.StopRecording();
            }
        }


        void wi_RecordingStopped(object sender, EventArgs e)
        {
            wi.Dispose();
            wfw.Close();
        }


    }
}
