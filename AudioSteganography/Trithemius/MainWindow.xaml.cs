﻿using System.Windows;
using Trithemius.Views;

namespace Trithemius
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.MainFrame.Content = new UploadCareerFileView();
        }
    }
}
