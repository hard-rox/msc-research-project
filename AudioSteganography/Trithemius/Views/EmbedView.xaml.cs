﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Win32;
using Steganography;

namespace Trithemius.Views
{
    /// <summary>
    /// Interaction logic for EmbedView.xaml
    /// </summary>
    public partial class EmbedView : Page
    {
        private WavFile _careerFile;
        private string _secretFilePath;
        public EmbedView(WavFile careerFile)
        {
            _careerFile = careerFile;
            InitializeComponent();
            FileNameLabel.Content = _careerFile.FileName;
            FileSizeLabel.Content = "File Size: " + (_careerFile.FileSize / 1048576.0).ToString("##.###") + " MB";
            EmbedCapacityLabel.Content = "Embed up to: " + (_careerFile.GetEmbedCapacity() / 1024.0).ToString("##.###") + "KB";
            SecretTextBox.MaxLength = _careerFile.GetEmbedCapacity();
            EmbedButton.IsEnabled = false;
        }

        private void ResetEmbedButton()
        {
            var secretFileSize = !string.IsNullOrEmpty(_secretFilePath) ? new FileInfo(_secretFilePath).Length : SecretTextBox.Text.Length;
            if ((!string.IsNullOrEmpty(_secretFilePath) || SecretTextBox.Text.Length > 0) &&
                (PasswordTextBox.Password.Length > 0 && secretFileSize <= _careerFile.GetEmbedCapacity()))
            {
                EmbedButton.IsEnabled = true;
                return;
            }
            EmbedButton.IsEnabled = false;
        }

        private void FileSelectorBorder_OnDrop(object sender, DragEventArgs e)
        {
            if (!string.IsNullOrEmpty(SecretTextBox.Text.Trim()))
            {
                var result = MessageBox.Show("You can only embed text message or file. Text message will be discurded. Are you sure?",
                    "Text or File", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No) return;
                SecretTextBox.Text = string.Empty;
            }
            var fileNames = (string[])e.Data.GetData(DataFormats.FileDrop);
            // ReSharper disable once PossibleNullReferenceException
            _secretFilePath = fileNames[0];
            EmbededFileStackPanel.Visibility = Visibility.Visible;
            EmbededFileNameLabel.Content = Path.GetFileName(fileNames[0]);
            var secretFileSize = new FileInfo(_secretFilePath).Length;
            if (secretFileSize > _careerFile.GetEmbedCapacity())
            {
                EmbedededFileSizeLebel.Content = "File is too large to embed. Try new career file.";
            }
            else
            {
                EmbedededFileSizeLebel.Content = "File size: " + new FileInfo(_secretFilePath).Length / 1024 + "KB";
            }
            ResetEmbedButton();
        }

        private void SecretFileDialogButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SecretTextBox.Text.Trim()))
            {
                var result = MessageBox.Show("You can only embed text message or file. Text message will be discurded. Are you sure?",
                    "Text or File", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No) return;
                SecretTextBox.Text = string.Empty;
            }

            var openFileDialog = new OpenFileDialog { Filter = "Any file (*.*)|*.*" };
            if (openFileDialog.ShowDialog() != true)
            {
                return;
            }
            _secretFilePath = openFileDialog.FileName;
            EmbededFileStackPanel.Visibility = Visibility.Visible;
            EmbededFileNameLabel.Content = Path.GetFileName(openFileDialog.FileName);
            var secretFileSize = new FileInfo(_secretFilePath).Length;
            if (secretFileSize > _careerFile.GetEmbedCapacity())
            {
                EmbedededFileSizeLebel.Content = "File is too large to embed. Try new career file.";
            }
            else
            {
                EmbedededFileSizeLebel.Content = "File size: " + new FileInfo(_secretFilePath).Length / 1024 + "KB";
            }
            ResetEmbedButton();
        }

        private void EmbedButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var steganographer = new AudioSteganographer();
                var message = !string.IsNullOrEmpty(_secretFilePath)
                    ? new SecretMessage().CreateFileMessage(_secretFilePath)
                    : new SecretMessage().CreateTextMessage(SecretTextBox.Text);

                var watch = Stopwatch.StartNew();
                var result = steganographer.Embed(_careerFile, message, PasswordTextBox.Password);
                watch.Stop();

                var navService = NavigationService.GetNavigationService(this);
                // ReSharper disable once PossibleNullReferenceException
                navService.Navigate(new SaveFileView(result, message, watch.ElapsedMilliseconds));
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void SecretTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(_secretFilePath))
            {
                var result = MessageBox.Show("You can only embed text or file. File will be discurded. Are you sure?",
                    "Text or File", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No) return;
                _secretFilePath = string.Empty;
            }
            if (SecretTextBox.Text.Length > _careerFile.GetEmbedCapacity())
            {
                EmbedededFileSizeLebel.Content = "Text is too large to embed. Try new career file.";
                
            }
            if (SecretTextBox.Text.Length > 0 && string.IsNullOrEmpty(_secretFilePath))
            {
                EmbededFileNameLabel.Content = "Text";
                EmbededFileStackPanel.Visibility = Visibility.Visible;
                EmbedededFileSizeLebel.Content = "Text size: " + SecretTextBox.Text.Length + "Bytes";
            }
            else
            {
                EmbededFileStackPanel.Visibility = Visibility.Hidden;
                EmbedededFileSizeLebel.Content = string.Empty;
            }
            ResetEmbedButton();
        }

        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            // ReSharper disable once PossibleNullReferenceException
            this.NavigationService.GoBack();
        }

        private void PasswordTextBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            ResetEmbedButton();
        }
    }
}
