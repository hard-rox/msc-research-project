﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Steganography;

namespace Trithemius.Views
{
    /// <summary>
    /// Interaction logic for ExtractView.xaml
    /// </summary>
    public partial class ExtractView : Page
    {
        private WavFile _careerFile;
        public ExtractView(WavFile careerFile)
        {
            InitializeComponent();

            _careerFile = careerFile;
            FileNameLabel.Content = _careerFile.FileName;
            FileSizeLabel.Content = "File Size: " + (_careerFile.FileSize / 1048576.0).ToString("##.###") + " MB";
            ExtractButton.IsEnabled = false;
        }

        private void ExtractButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var steganographer = new AudioSteganographer();
                var watch = Stopwatch.StartNew();
                var result = steganographer.Extract(_careerFile, PasswordTextBox.Password);
                watch.Stop();

                var navService = NavigationService.GetNavigationService(this);
                // ReSharper disable once PossibleNullReferenceException
                navService.Navigate(new SaveFileView(_careerFile, result, watch.ElapsedMilliseconds));
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void PasswordTextBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            if (PasswordTextBox.Password.Length > 0) ExtractButton.IsEnabled = true;
        }

        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            // ReSharper disable once PossibleNullReferenceException
            this.NavigationService.GoBack();
        }
    }
}
