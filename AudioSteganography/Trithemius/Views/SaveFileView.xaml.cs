﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using Steganography;

namespace Trithemius.Views
{
    /// <summary>
    /// Interaction logic for SaveFileView.xaml
    /// </summary>
    public partial class SaveFileView : Page
    {
        private readonly WavFile _careerFile;
        private readonly SecretMessage _message;
        private readonly string _caller;
        public SaveFileView(WavFile careerFile, SecretMessage message, long executionTime, [System.Runtime.CompilerServices.CallerMemberName] string member = "")
        {
            InitializeComponent();
            SaveButton.Content = _caller == "EmbedButton_OnClick" ? "Save Career File" : "Save Secret File";
            _caller = member;
            _careerFile = careerFile;
            _message = message;
            FileNameLabel.Content = careerFile.FileName;
            FileSizeLabel.Content = "File Size: " + (careerFile.FileSize / 1048576) + " MB";

            EmbededFileNameLabel.Content = message.Extension + " File";
            TimeLebel.Content = "Processed in: " + (executionTime / 1000.0) + " Seconds.";
        }

        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            // ReSharper disable once PossibleNullReferenceException
            this.NavigationService.GoBack();
        }

        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_caller == "EmbedButton_OnClick")
            {
                var saveFileDialog = new SaveFileDialog { Filter = "WAV file (*.wav)|*.wav" };
                if (saveFileDialog.ShowDialog() != true) return;
                saveFileDialog.DefaultExt = "wav";
                if (Path.HasExtension(saveFileDialog.FileName))
                {
                    saveFileDialog.FileName =
                        saveFileDialog.FileName.Replace(Path.GetExtension(saveFileDialog.FileName),
                            "." + saveFileDialog.DefaultExt);
                }
                _careerFile.WriteFile(saveFileDialog.FileName);
            }
            else
            {
                var saveFileDialog = new SaveFileDialog();
                if (saveFileDialog.ShowDialog() != true) return;
                saveFileDialog.DefaultExt = _message.Extension == "text" ? "txt" : _message.Extension;
                if (Path.HasExtension(saveFileDialog.FileName))
                {
                    saveFileDialog.FileName =
                        saveFileDialog.FileName.Replace(Path.GetExtension(saveFileDialog.FileName),
                            "." + saveFileDialog.DefaultExt);
                }
                else
                {
                    saveFileDialog.FileName =
                        saveFileDialog.FileName + "." + saveFileDialog.DefaultExt;
                }
                File.WriteAllBytes(saveFileDialog.FileName, _message.MessageBytes);
            }
        }
    }
}
