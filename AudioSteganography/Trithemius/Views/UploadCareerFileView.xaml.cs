﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Win32;
using Steganography;
using Path = System.IO.Path;

namespace Trithemius.Views
{
    /// <inheritdoc />
    /// <summary>
    /// Interaction logic for UploadCareerFileView.xaml
    /// </summary>
    public partial class UploadCareerFileView : Page
    {
        private string _filePath;

        public UploadCareerFileView()
        {
            InitializeComponent();
        }

        private void EmbedButton_OnClick(object sender, RoutedEventArgs e)
        {
            var careerFile = new WavFile(_filePath);

            var navService = NavigationService.GetNavigationService(this);
            // ReSharper disable once PossibleNullReferenceException
            navService.Navigate(new EmbedView(careerFile));
        }

        private void FileDialogButton_OnClick(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog {Filter = "Wav Audio File (*.wav)|*.wav"};
            if (openFileDialog.ShowDialog() != true)
            {
                EmbedButton.IsEnabled = false;
                ExtractButton.IsEnabled = false;
                return;
            }
            _filePath = openFileDialog.FileName;
            FileNameStackPanel.Visibility = Visibility.Visible;
            FileNameLabel.Content = Path.GetFileName(openFileDialog.FileName);
            EmbedButton.IsEnabled = true;
            ExtractButton.IsEnabled = true;
        }

        private void FileSelectorBorder_OnDrop(object sender, DragEventArgs e)
        {
            var fileNames = (string[])e.Data.GetData(DataFormats.FileDrop);
            // ReSharper disable once PossibleNullReferenceException
            if (Path.GetExtension(fileNames[0]) != ".wav")
            {
                EmbedButton.IsEnabled = false;
                ExtractButton.IsEnabled = false;
                MessageBox.Show("Not .wav format. Accepts only .wav file as career.", "Unsupported format",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            _filePath = fileNames[0];
            FileNameStackPanel.Visibility = Visibility.Visible;
            FileNameLabel.Content = Path.GetFileName(fileNames[0]);
            EmbedButton.IsEnabled = true;
            ExtractButton.IsEnabled = true;
        }

        private void ExtractButton_OnClick(object sender, RoutedEventArgs e)
        {
            var careerFile = new WavFile(_filePath);

            var navService = NavigationService.GetNavigationService(this);
            // ReSharper disable once PossibleNullReferenceException
            navService.Navigate(new ExtractView(careerFile));
        }
    }
}
