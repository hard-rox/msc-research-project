\contentsline {paragraph}{A research report}{i}{section*.1}% 
\contentsline {paragraph}{}{ii}{section*.2}% 
\contentsline {paragraph}{}{ii}{section*.3}% 
\contentsline {paragraph}{}{ii}{section*.4}% 
\contentsline {paragraph}{}{iii}{section*.5}% 
\contentsline {paragraph}{}{iii}{section*.6}% 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Introduction}{1}{section.1.1}% 
\contentsline {paragraph}{}{1}{section*.11}% 
\contentsline {section}{\numberline {1.2}Background of the research}{1}{section.1.2}% 
\contentsline {paragraph}{}{1}{section*.12}% 
\contentsline {paragraph}{}{1}{section*.13}% 
\contentsline {paragraph}{}{1}{section*.14}% 
\contentsline {section}{\numberline {1.3}Objective of the research}{1}{section.1.3}% 
\contentsline {paragraph}{}{1}{section*.15}% 
\contentsline {paragraph}{}{2}{section*.16}% 
\contentsline {chapter}{\numberline {2}Theory}{3}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Introduction}{3}{section.2.1}% 
\contentsline {paragraph}{}{3}{section*.17}% 
\contentsline {section}{\numberline {2.2}History of Steganograpy}{3}{section.2.2}% 
\contentsline {paragraph}{}{3}{section*.18}% 
\contentsline {paragraph}{}{3}{section*.19}% 
\contentsline {section}{\numberline {2.3}Steganography in Digital Media}{4}{section.2.3}% 
\contentsline {subsection}{\numberline {2.3.1}Text Steganography}{4}{subsection.2.3.1}% 
\contentsline {paragraph}{}{4}{section*.20}% 
\contentsline {subsection}{\numberline {2.3.2}Image Steganography}{5}{subsection.2.3.2}% 
\contentsline {paragraph}{}{5}{section*.21}% 
\contentsline {subsection}{\numberline {2.3.3}Network Steganography}{5}{subsection.2.3.3}% 
\contentsline {paragraph}{}{5}{section*.22}% 
\contentsline {subsection}{\numberline {2.3.4}Audio Steganography}{5}{subsection.2.3.4}% 
\contentsline {paragraph}{}{5}{section*.23}% 
\contentsline {subsection}{\numberline {2.3.5}Video Steganography}{6}{subsection.2.3.5}% 
\contentsline {paragraph}{}{6}{section*.24}% 
\contentsline {section}{\numberline {2.4}Methods of Audio Steganography}{6}{section.2.4}% 
\contentsline {section}{\numberline {2.5}Comparison of techniques}{7}{section.2.5}% 
\contentsline {section}{\numberline {2.6}General Steganography Algorithm}{7}{section.2.6}% 
\contentsline {subsection}{\numberline {2.6.1}Embedding Process}{8}{subsection.2.6.1}% 
\contentsline {subsection}{\numberline {2.6.2}Extracting Process}{8}{subsection.2.6.2}% 
\contentsline {chapter}{\numberline {3}Proposed System Model}{9}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Introduction}{9}{section.3.1}% 
\contentsline {paragraph}{}{9}{section*.25}% 
\contentsline {section}{\numberline {3.2}WAV File structure}{9}{section.3.2}% 
\contentsline {paragraph}{}{9}{section*.26}% 
\contentsline {paragraph}{}{9}{section*.27}% 
\contentsline {paragraph}{}{9}{section*.28}% 
\contentsline {section}{\numberline {3.3}Proposed Approach}{10}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Approach}{10}{subsection.3.3.1}% 
\contentsline {paragraph}{}{10}{section*.29}% 
\contentsline {paragraph}{}{11}{section*.30}% 
\contentsline {paragraph}{}{11}{section*.31}% 
\contentsline {subsection}{\numberline {3.3.2}Embedding Process}{12}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}Extracting Process}{13}{subsection.3.3.3}% 
\contentsline {subsection}{\numberline {3.3.4}Embedding Algorithm}{14}{subsection.3.3.4}% 
\contentsline {subsection}{\numberline {3.3.5}Extracting Algorithm}{14}{subsection.3.3.5}% 
\contentsline {chapter}{\numberline {4}Experiments \& Results}{15}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Introduction}{15}{section.4.1}% 
\contentsline {paragraph}{}{15}{section*.32}% 
\contentsline {section}{\numberline {4.2}Experimenting Application}{15}{section.4.2}% 
\contentsline {paragraph}{}{15}{section*.33}% 
\contentsline {subsection}{\numberline {4.2.1}Upload Career window}{15}{subsection.4.2.1}% 
\contentsline {paragraph}{}{15}{section*.34}% 
\contentsline {subsection}{\numberline {4.2.2}Embed window}{16}{subsection.4.2.2}% 
\contentsline {paragraph}{}{16}{section*.35}% 
\contentsline {subsection}{\numberline {4.2.3}Extract window}{16}{subsection.4.2.3}% 
\contentsline {paragraph}{}{16}{section*.36}% 
\contentsline {subsection}{\numberline {4.2.4}Save File window}{16}{subsection.4.2.4}% 
\contentsline {paragraph}{}{16}{section*.37}% 
\contentsline {section}{\numberline {4.3}Experimenting Environment}{17}{section.4.3}% 
\contentsline {paragraph}{}{17}{section*.38}% 
\contentsline {section}{\numberline {4.4}Evaluating of results}{17}{section.4.4}% 
\contentsline {subsection}{\numberline {4.4.1}Comparison of Result}{17}{subsection.4.4.1}% 
\contentsline {paragraph}{}{17}{section*.39}% 
\contentsline {subsection}{\numberline {4.4.2}Comparing input and output career spectrum}{18}{subsection.4.4.2}% 
\contentsline {paragraph}{}{18}{section*.40}% 
\contentsline {section}{\numberline {4.5}Conclusion}{19}{section.4.5}% 
\contentsline {paragraph}{}{19}{section*.41}% 
\contentsline {chapter}{\numberline {5}Conclusion \& Future Direction}{20}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Conclusion}{20}{section.5.1}% 
\contentsline {paragraph}{}{20}{section*.42}% 
\contentsline {section}{\numberline {5.2}Limitation}{20}{section.5.2}% 
\contentsline {paragraph}{}{20}{section*.43}% 
\contentsline {section}{\numberline {5.3}Future work direction}{20}{section.5.3}% 
\contentsline {paragraph}{}{20}{section*.44}% 
